import pandas as pd
from sklearn.model_selection import train_test_split
from nltk.tokenize import word_tokenize
import gensim.models.keyedvectors as word2vec
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.metrics import accuracy_score, confusion_matrix, recall_score, f1_score
from sklearn import svm
import numpy as np
from sklearn.ensemble import AdaBoostClassifier
from nltk.corpus import stopwords
from sklearn.ensemble import RandomForestClassifier

stop_words = set(stopwords.words('english'))

model = word2vec.KeyedVectors.load_word2vec_format('../GoogleNews-vectors-negative300.bin', binary=True)




def document_vector(word2vec_model, doc):
    doc = [word for word in doc if word in model.vocab]
    try:
        return np.mean(model[doc], axis=0)
    except:
        return np.nan


def preprocess(text):
    text = text.lower()
    doc = word_tokenize(text)
    doc = [word for word in doc if word not in stop_words]
    doc = [word for word in doc if word.isalpha()]
    return doc

def getEmbededVector(row):
    x = []
    for doc in row:
        x.append(document_vector(model, doc))

def has_vector_representation(word2vec_model, doc):
    return not all(word not in word2vec_model.vocab for word in doc)

def filter_docs(corpus, texts, condition_on_doc):
    number_of_docs = len(corpus)

    if texts is not None:
        texts = [text for (text, doc) in zip(texts, corpus)
                 if condition_on_doc(doc)]

    corpus = [doc for doc in corpus if condition_on_doc(doc)]

    print("{} docs removed".format(number_of_docs - len(corpus)))

    return (corpus, texts)

def basic_clean(text):
  wnl = nltk.stem.WordNetLemmatizer()
  stopwords = nltk.corpus.stopwords.words('english')
  words = re.sub(r'[^\w\s]', '', text).split()
  return [wnl.lemmatize(word) for word in words if word not in stopwords]

fake = pd.read_csv('datasets/FakeAndReal/Fake.csv', index_col=None, header=0)
true = pd.read_csv('datasets/FakeAndReal/True.csv', index_col=None, header=0)
cleansed_data = []
for data in true.text:
    if "@realDonaldTrump : - " in data:
        cleansed_data.append(data.split("@realDonaldTrump : - ")[1])
    elif "(Reuters) -" in data:
        cleansed_data.append(data.split("(Reuters) - ")[1])
    else:
        cleansed_data.append(data)
true["text"] = cleansed_data


fake['Label'] = 0
true['Label'] = 1
fake['Sentences'] = fake['title'] + ' ' + fake['text']
true['Sentences'] = true['title'] + ' ' + true['text']
df = pd.concat([fake, true])

df = df.dropna()  # drop NAN values






df['preproces'] = df.apply(lambda row: preprocess(row['Sentences']), axis=1)
df['word2vec'] = df.apply(lambda row: document_vector(model,row['preproces']), axis=1)
df = df.dropna()
labels = df.Label

X = np.array(list(df['word2vec'][1:]))
print(X)
print(len(X))
print(len(X[0]))
y = df['Label'][1:].values
x_train,x_test,y_train,y_test=train_test_split(X, y, test_size=0.2, random_state=7)

svmc = svm.SVC(kernel="linear")
svmc.fit(x_train, y_train)

y_predSVMC=svmc.predict(x_test)
score=accuracy_score(y_test,y_predSVMC)
recal = recall_score(y_test, y_predSVMC)
f1 = f1_score(y_test, y_predSVMC)
print(f'SVM Classifier Accuracy: {round(score*100,2)}%')
print(f'SVM Classifier recall: {round(recal * 100, 2)}%')
print(f'SVM Classifier f1: {round(f1 * 100, 2)}%')



clf = AdaBoostClassifier(n_estimators=300, random_state=0)
clf.fit(x_train, y_train)
y_predADA=clf.predict(x_test)
score=accuracy_score(y_test,y_predADA)
recal = recall_score(y_test, y_predADA)
f1 = f1_score(y_test, y_predADA)
print(f'ADA Classifier Accuracy: {round(score*100,2)}%')
print(f'ADA Classifier recall: {round(recal * 100, 2)}%')
print(f'ADA Classifier f1: {round(f1 * 100, 2)}%')

forr = RandomForestClassifier(max_depth=500, random_state=0)
forr.fit(x_train, y_train)

y_predFOR = forr.predict(x_test)
score = accuracy_score(y_test, y_predFOR)
recal = recall_score(y_test, y_predFOR)
f1 = f1_score(y_test, y_predFOR)


print(f'RandomForestClassifier Accuracy: {round(score * 100, 2)}%')
print(f'RandomForestClassifier recall: {round(recal * 100, 2)}%')
print(f'RandomForestClassifier f1: {round(f1 * 100, 2)}%')