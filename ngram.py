import sklearn.feature_extraction.text
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn import svm
from nltk.corpus import stopwords
from sklearn.metrics import recall_score, f1_score
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier

stopwords = stopwords.words('english')


def checkExperiments(X,y, dataset):

    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=7)
    ngram_size = dataset
    vect = CountVectorizer(ngram_range=(ngram_size, ngram_size), stop_words=stopwords)
    bag_train = vect.fit_transform(x_train)
    bag_check = vect.transform(x_test)

    f1_arr = []
    acc_arr = []
    recal_arr = []
    forr = RandomForestClassifier(max_depth=500, random_state=0)
    forr.fit(bag_train, y_train)

    y_predFOR = forr.predict(bag_check)
    score = accuracy_score(y_test, y_predFOR)
    recal = recall_score(y_test, y_predFOR)
    f1 = f1_score(y_test, y_predFOR)
    f1_arr.append(f1)
    acc_arr.append(score)
    recal_arr.append(recal)

    print(f'RandomForestClassifier Accuracy: {round(score * 100, 2)}%')
    print(f'RandomForestClassifier recall: {round(recal * 100, 2)}%')
    print(f'RandomForestClassifier f1: {round(f1 * 100, 2)}%')



    max_iter = [10,50,100,200]
    f1_arr = []
    acc_arr = []
    recal_arr = []
    pac = PassiveAggressiveClassifier(max_iter=50)
    pac.fit(bag_train, y_train)
    y_predPAC = pac.predict(bag_check)
    score = accuracy_score(y_test, y_predPAC)
    recal = recall_score(y_test, y_predPAC)
    f1 = f1_score(y_test, y_predPAC)
    f1_arr.append(f1)
    acc_arr.append(score)
    recal_arr.append(recal)
    print(f'Passive Aggresive Classifier Accuracy: {round(score * 100, 2)}%')
    print(f'Passive Aggresive Classifier recall: {round(recal * 100, 2)}%')
    print(f'Passive Aggresive Classifier f1: {round(f1 * 100, 2)}%')


    # Predict on the test set and calculate accuracy of SVM Classifier
    f1_arr = []
    acc_arr = []
    recal_arr = []

    # Initialize a SVMClassifier
    svmc = svm.SVC(kernel="linear", C=1)
    svmc.fit(bag_train, y_train)
    y_predSVMC = svmc.predict(bag_check)
    score = accuracy_score(y_test, y_predSVMC)
    recal = recall_score(y_test, y_predSVMC)
    f1 = f1_score(y_test, y_predSVMC)
    f1_arr.append(f1)
    acc_arr.append(score)
    recal_arr.append(recal)
    print(f'SVM Classifier Accuracy: {round(score * 100, 2)}%')
    print(f'SVM Classifier recall: {round(recal * 100, 2)}%')
    print(f'SVM Classifier f1: {round(f1 * 100, 2)}%')

    f1_arr = []
    acc_arr = []
    recal_arr = []
    clf = AdaBoostClassifier(n_estimators=300, random_state=0)
    clf.fit(bag_train, y_train)

    y_predADA = clf.predict(bag_check)
    score = accuracy_score(y_test, y_predADA)
    recal = recall_score(y_test, y_predADA)
    f1 = f1_score(y_test, y_predADA)
    f1_arr.append(f1)
    acc_arr.append(score)
    recal_arr.append(recal)
    print(f'ADA Classifier Accuracy: {round(score * 100, 2)}%')
    print(f'ADA Classifier recall: {round(recal * 100, 2)}%')
    print(f'ADA Classifier f1: {round(f1 * 100, 2)}%')






#
# df = pd.read_csv('datasets/FakeAndReal/train.csv', sep=",")
# df = df.dropna() # drop NAN values
# labels=df.label # 0 - unreliable 1 -reliable
# X = df['text']
# y = labels
# checkExperiments(X,y,'aa')
# df = pd.read_csv('datasets/ForFake/train.csv', index_col=None, header=0)
#
# # train = train.sample(frac=1).reset_index(drop=True)
# # print(train.head())
# # train['text'].str.len().hist(by=train['label'])
# df = df.dropna()
# X = df['text']
# y = df.label
# checkExperiments(X,y,2)
# checkExperiments(X,y,3)

#
fake = pd.read_csv('datasets/FakeAndReal/Fake.csv', index_col=None, header=0)
true = pd.read_csv('datasets/FakeAndReal/True.csv', index_col=None, header=0)
cleansed_data = []
for data in true.text:
    if "@realDonaldTrump : - " in data:
        cleansed_data.append(data.split("@realDonaldTrump : - ")[1])
    elif "(Reuters) -" in data:
        cleansed_data.append(data.split("(Reuters) - ")[1])
    else:
        cleansed_data.append(data)
true["text"] = cleansed_data

cleansed_data = []
for data in fake.text:
    if "21st Century Wire says" in data:
        cleansed_data.append(data.replace("21st Century Wire says ", '').replace("FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV", '').replace("21WIRE", ''))
    elif "FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV" in data:

        cleansed_data.append(data.replace("FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV", '').replace("21WIRE", ''))

    elif "21WIRE" in data:
        cleansed_data.append(data.replace("21WIRE", ''))

    else:
        cleansed_data.append(data)
fake["text"] = cleansed_data
fake['Label'] = 0
true['Label'] = 1
fake['Sentences'] = fake['title'] + ' ' + fake['text']
true['Sentences'] = true['title'] + ' ' + true['text']
train = pd.concat([fake, true])
X = train['Sentences']
y = train['Label']
checkExperiments(X,y,2)
checkExperiments(X,y,3)
