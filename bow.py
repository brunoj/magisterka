import numpy as np
import pandas as pd
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import LinearSVC
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.metrics import recall_score, f1_score
from sklearn import svm
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVR

def checkExperiments(X,y):
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=7)

    tfidf_vectorizer = TfidfVectorizer(use_idf=True)
    tfidf_train = tfidf_vectorizer.fit_transform(x_train)

    tfidf_test = tfidf_vectorizer.transform(x_test)


    f1_arr = []
    acc_arr = []
    recal_arr = []
    forr = RandomForestClassifier(max_depth=500, random_state=0)
    cv = KFold(n_splits=5, random_state=7, shuffle=False)
    scores = []
    for train_index, test_index in cv.split(X):
        print("Train Index: ", train_index, "\n")
        print("Test Index: ", test_index)

        x_train, x_test, y_train, y_test = X[train_index], X[test_index], y[train_index], y[test_index]
        forr.fit(x_train, y_train)
        scores.append(accuracy_score(y_test, y_test))
    forr.fit(tfidf_train, y_train)

    y_predFOR = forr.predict(tfidf_test)
    score = accuracy_score(y_test, y_predFOR)
    recal = recall_score(y_test, y_predFOR)
    f1 = f1_score(y_test, y_predFOR)
    f1_arr.append(f1)
    acc_arr.append(score)
    recal_arr.append(recal)

    print(f'RandomForestClassifier Accuracy: {round(score * 100, 2)}%')
    print(f'RandomForestClassifier recall: {round(recal * 100, 2)}%')
    print(f'RandomForestClassifier f1: {round(f1 * 100, 2)}%')
    df = pd.DataFrame({
        'max_depth': max_depth,
        'accuracy': acc_arr,
        'recall': recal_arr,
        'f1': f1_arr,
    })

    df.to_csv('results/FakeAndRealWithoutWire/RandomForest.csv', index=False)


    max_iter = 200
    f1_arr = []
    acc_arr = []
    recal_arr = []
    pac = PassiveAggressiveClassifier(max_iter=200)
    pac.fit(tfidf_train, y_train)
    y_predPAC = pac.predict(tfidf_test)
    score = accuracy_score(y_test, y_predPAC)
    recal = recall_score(y_test, y_predPAC)
    f1 = f1_score(y_test, y_predPAC)
    f1_arr.append(f1)
    acc_arr.append(score)
    recal_arr.append(recal)
    print(f'Passive Aggresive Classifier Accuracy: {round(score * 100, 2)}%')
    print(f'Passive Aggresive Classifier recall: {round(recal * 100, 2)}%')
    print(f'Passive Aggresive Classifier f1: {round(f1 * 100, 2)}%')
    df = pd.DataFrame({
        'max_iter': max_iter,
        'accuracy': acc_arr,
        'recall': recal_arr,
        'f1': f1_arr,
    })

    df.to_csv('results/FakeAndRealWithoutWire/PasiveAggresive.csv', index=False)
    # Predict on the test set and calculate accuracy of SVM Classifier


        # Initialize a SVMClassifier
    svmc = svm.SVC(kernel="linear", C=0.1)
    svmc.fit(tfidf_train, y_train)
    y_predSVMC = svmc.predict(tfidf_test)
    score = accuracy_score(y_test, y_predSVMC)
    recal = recall_score(y_test, y_predSVMC)
    f1 = f1_score(y_test, y_predSVMC)
    f1_arr.append(f1)
    acc_arr.append(score)
    recal_arr.append(recal)
    print(f'SVM Classifier Accuracy: {round(score * 100, 2)}%')
    print(f'SVM Classifier recall: {round(recal * 100, 2)}%')
    print(f'SVM Classifier f1: {round(f1 * 100, 2)}%')
    df = pd.DataFrame({
        'pemalty': penaltys,
        'accuracy': acc_arr,
        'recall': recal_arr,
        'f1': f1_arr,
    })

    df.to_csv('results/FakeAndRealWithoutWire/SVC.csv', index=False)
    n_estimator = [10,50,100,200,300,1000]
    f1_arr = []
    acc_arr = []
    recal_arr = []
    for x in n_estimator:
    clf = AdaBoostClassifier(n_estimators=x, random_state=0)
    clf.fit(tfidf_train, y_train)

    y_predADA = clf.predict(tfidf_test)
    score = accuracy_score(y_test, y_predADA)
    recal = recall_score(y_test, y_predADA)
    f1 = f1_score(y_test, y_predADA)
    f1_arr.append(f1)
    acc_arr.append(score)
    recal_arr.append(recal)
    print(f'ADA Classifier Accuracy: {round(score * 100, 2)}%')
    print(f'Passive Aggresive Classifier Accuracy: {round(score * 100, 2)}%')
    print(f'Passive Aggresive Classifier recall: {round(recal * 100, 2)}%')
    print(f'Passive Aggresive Classifier f1: {round(f1 * 100, 2)}%')
    df = pd.DataFrame({
        'n_estimator': n_estimator,
        'accuracy': acc_arr,
        'recall': recal_arr,
        'f1': f1_arr,
    })

    df.to_csv('results/FakeAndRealWithoutWire/ADA.csv', index=False)

# df = pd.read_csv('fake-news/_news.csv', sep=",")
# df = pd.read_csv('datasets/FA.csv', sep=",")

fake = pd.read_csv('datasets/FakeAndReal/Fake.csv', index_col=None, header=0)
true = pd.read_csv('datasets/FakeAndReal/True.csv', index_col=None, header=0)
cleansed_data = []
for data in true.text:
    if "@realDonaldTrump : - " in data:
        cleansed_data.append(data.split("@realDonaldTrump : - ")[1])
    elif "(Reuters) -" in data:
        cleansed_data.append(data.split("(Reuters) - ")[1])
    else:
        cleansed_data.append(data)
true["text"] = cleansed_data

cleansed_data = []
for data in fake.text:
    if "21st Century Wire says" in data:
        cleansed_data.append(data.replace("21st Century Wire says ", '').replace("FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV", '').replace("21WIRE", ''))
    elif "FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV" in data:

        cleansed_data.append(data.replace("FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV", '').replace("21WIRE", ''))

    elif "21WIRE" in data:
        cleansed_data.append(data.replace("21WIRE", ''))

    else:
        cleansed_data.append(data)
fake["text"] = cleansed_data
fake['Label'] = 0
true['Label'] = 1
fake['Sentences'] = fake['title'] + ' ' + fake['text']
true['Sentences'] = true['title'] + ' ' + true['text']
train = pd.concat([fake, true])


#
# train = pd.read_csv('datasets/ForFake/train.csv', index_col=None, header=0)
# train['Sentences'] = train['title'] + ' ' + train['text']

train = train.sample(frac=1).reset_index(drop=True)





# train = train.sample(frac=1).reset_index(drop=True)

# train['text'].replace('', np.nan, inplace=True)
# train['text']=train.apply(lambda x: x.split("(Reuters) - ",1)[1],axis=1)
print(train.head())
train = train.dropna() # drop NAN valueslabels=df.labels # 0 - unreliable 1 -reliable
X = train['Sentences']
y = train['Label']
checkExperiments(X,y, 'ForFake')

# df = pd.read_csv('datasets/FA.csv', sep=",")
# df = df.dropna() # drop NAN values
# labels=df.labels # 0 - unreliable 1 -reliable
# X = df['article_content']
# y = labels