import pandas as pd
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import re
import nltk
from gensim.parsing.preprocessing import preprocess_string, strip_tags, strip_punctuation, strip_multiple_whitespaces, strip_numeric, remove_stopwords, strip_short # Preprocesssing

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer,WordNetLemmatizer
from nltk.tokenize import word_tokenize
import pyLDAvis.gensim
import gensim
from tabulate import tabulate
from wordcloud import WordCloud, STOPWORDS

# from wordcloud import WordCloud, STOPWORDS

from collections import defaultdict, Counter
import string
from sklearn.feature_extraction.text import CountVectorizer
def basic_clean(text):
  wnl = nltk.stem.WordNetLemmatizer()
  stopwords = nltk.corpus.stopwords.words('english')
  words = re.sub(r'[^\w\s]', '', text).split()
  return [wnl.lemmatize(word) for word in words if word not in stopwords]

fake = pd.read_csv('datasets/FakeAndReal/Fake.csv', index_col=None, header=0)
true = pd.read_csv('datasets/FakeAndReal/True.csv', index_col=None, header=0)
cleansed_data = []
for data in true.text:
    if "@realDonaldTrump : - " in data:
        cleansed_data.append(data.split("@realDonaldTrump : - ")[1])
    elif "(Reuters) -" in data:
        cleansed_data.append(data.split("(Reuters) - ")[1])
    else:
        cleansed_data.append(data)
# true["text"] = cleansed_data


cleansed_data = []
# for data in fake.text:
#     if "21st Century Wire says" in data:
#         cleansed_data.append(data.replace("21st Century Wire says ", '').replace("FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV", '').replace("21WIRE", ''))
#     elif "FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV" in data:
#
#         cleansed_data.append(data.replace("FILESSUPPORT OUR WORK BY SUBSCRIBING & BECOMING A MEMBER @21WIRE.TV", '').replace("21WIRE", ''))
#
#     elif "21WIRE" in data:
#         cleansed_data.append(data.replace("21WIRE", ''))
#
#     else:
#         cleansed_data.append(data)
# fake["text"] = cleansed_data

fake['Label'] = 0
true['Label'] = 1
fake['Sentences'] = fake['title'] + ' ' + fake['text']
true['Sentences'] = true['title'] + ' ' + true['text']
train = pd.concat([fake, true])
# train = train.sample(frac=1).reset_index(drop=True)

def avg_text_length(dataframe):
    rowCounts = [len(x.split()) for x in dataframe["text"].tolist()]
    avgCount = sum(rowCounts) // len(rowCounts)

    return avgCount



true_lengths = pd.Series([len(x.split()) for x in true["text"].tolist()])
fake_lengths = pd.Series([len(x.split()) for x in fake["text"].tolist()])

# plt.figure(figsize = (15,10))
# plt.hist(true_lengths, bins = 100, range = [0, 2000], color = "green", label = "true news")
# plt.hist(fake_lengths, bins = 100, range = [0, 2000], color = "red", alpha = 0.5, label = "fake news")
# plt.xlabel("Length of a news article in no. of words.", fontsize = 15)
# plt.ylabel("Number of articles with x word length.", fontsize = 15)
# plt.legend()
# plt.show()



# train = train.sample(frac=1).reset_index(drop=True)

# train['text'].replace('', np.nan, inplace=True)
# train['text']=train.apply(lambda x: x.split("(Reuters) - ",1)[1],axis=1)
# print(train.head())
# train = train.dropna() # drop NAN values
# print(train.size)

#
# true_word = basic_clean(''.join(str(true['text'].tolist())))
# #
# true_bigrams_series = (pd.Series(nltk.ngrams(true_word, 3)).value_counts())[:20]
# true_bigrams_series.sort_values().plot.barh(color='blue',  width=.9, figsize=(12, 8))
# plt.title('20 Most Frequently Occuring Trigrams')
# plt.ylabel('Trigram')
# plt.xlabel('# of Occurances')
# plt.show()
# #
# #
# true_word = basic_clean(''.join(str(fake['text'].tolist())))
#
# true_bigrams_series = (pd.Series(nltk.ngrams(true_word, 2)).value_counts())[:20]
# true_bigrams_series.sort_values().plot.barh(color='blue', width=.9, figsize=(12, 8))
# plt.title('20 Most Frequently Occuring Bigrams')
# plt.ylabel('Bigram')
# plt.xlabel('# of Occurances')
# plt.show()



# train[['Label']].hist(bins = 3)
# plt.bar(np.arange(len([0,1])), train.groupby(['Label']).size().values, 0.9,  color="blue")
# plt.xticks(np.arange(len([0,1])), [0,1])
# plt.show()
# train['totalwords'] = train['Sentences'].str.split().str.len()
# train['totalwords'].hist(by=train['Label'])
# #
# # train['Sentences'].str.split().apply(lambda x:[len(i) for i in x]).hist(by=train['Label'])
# #
# plt.show()import pandas as pd
# #
# # true_bigrams_series = (pd.Series(nltk.ngrams(true_word, 2)).value_counts())[:20]
# # true_bigrams_series.sort_values().plot.barh(color='blue', width=.9, figsize=(12, 8))
# # plt.title('20 Most Frequently Occuring Bigrams')
# # plt.ylabel('Bigram')
# # plt.xlabel('# of Occurances')
# # plt.show()
# #
# #
# # true_word = basic_clean(''.join(str(true['Sentences'].tolist())))
# #
# # true_bigrams_series = (pd.Series(nltk.ngrams(true_word, 2)).value_counts())[:20]
# # true_bigrams_series.sort_values().plot.barh(color='blue', width=.9, figsize=(12, 8))
# # plt.title('20 Most Frequently Occuring Bigrams')
# # plt.ylabel('Bigram')
# # plt.xlabel('# of Occurances')
# # plt.show()
#
#
#
# train[['Label']].hist(bins = 3)
# plt.bar(np.arange(len([0,1])), train.groupby(['Label']).size().values, 0.9,  color="blue")
# plt.xticks(np.arange(len([0,1])), [0,1])
# plt.show()

# train['number_of_words'] = train['text'].apply(lambda x: len(str(x).split(" ")))
# train['number_of_words'].hist(by=train['Label'])

#
# train['Sentences'].str.split().apply(lambda x:[len(i) for i in x]).hist(by=train['Label'])


# plt.show()

#
# stopwords

stopwords = set(STOPWORDS)


def show_wordcloud(data):
    wordcloud = WordCloud(
        background_color='black',
        stopwords=stopwords,
        max_words=100,
        max_font_size=30,
        scale=3,
        random_state=1)

    wordcloud = wordcloud.generate(str(data))

    plt.figure( figsize=(20,10), facecolor='k')
    plt.axis('off')

    plt.imshow(wordcloud)
    plt.show()
# show_wordcloud(fake['text'])
# show_wordcloud(true['text'])

#
# messages_fake=messages[messages.label==1].copy()
# messages_not_fake=messages[messages.label==0].copy()

# print(train)
# train['count'] = train['text'].str.count(' ') + 1
#
# class_count = train.groupby('labels').size()
# texts = train.groupby('count').size()
# print(class_count)
# print(texts)
# print(train.head())


train = pd.read_csv('datasets/ForFake/train.csv', index_col=None, header=0)

# train = train.sample(frac=1).reset_index(drop=True)
# print(train.head())
# train['text'].str.len().hist(by=train['label'])
train = train.dropna()

true = train.loc[train['label'] == 0]
fake = train.loc[train['label'] == 1]
# print(avg_text_length(true))
# print(avg_text_length(fake))
# true_lengths = pd.Series([len(x.split()) for x in true['text'].tolist()])
# fake_lengths = pd.Series([len(x.split()) for x in fake['text'].tolist()])
#
# plt.figure(figsize = (15,10))
# plt.hist(true_lengths, bins = 100, range = [0, 2000], color = "green", label = "true news")
# plt.hist(fake_lengths, bins = 100, range = [0, 2000], color = "red", alpha = 0.5, label = "fake news")
# plt.xlabel("Length of a news article in no. of words.", fontsize = 15)
# plt.ylabel("Number of articles with x word length.", fontsize = 15)
# plt.legend()
# plt.show()

# show_wordcloud(fake['text'])
# show_wordcloud(true['text'])



#
# train['Sentences'].str.split().apply(lambda x:[len(i) for i in x]).hist(by=train['Label'])
# plt.ylabel('Number of words in article')
# plt.xlabel('# of Occurances')
# #
#
# true_word = basic_clean(''.join(str(true['text'].tolist())))
# #
# true_bigrams_series = (pd.Series(nltk.ngrams(true_word, 2)).value_counts())[:20]
# true_bigrams_series.sort_values().plot.barh(color='blue',  width=.9, figsize=(12, 8))
# plt.title('20 Most Frequently Occuring Bigrams')
# plt.ylabel('Bigram')
# plt.xlabel('# of Occurances')
# plt.show()
# #
# #
# true_word = basic_clean(''.join(str(fake['text'].tolist())))
#
# true_bigrams_series = (pd.Series(nltk.ngrams(true_word, 2)).value_counts())[:20]
# true_bigrams_series.sort_values().plot.barh(color='blue', width=.9, figsize=(12, 8))
# plt.title('20 Most Frequently Occuring Bigrams')
# plt.ylabel('Bigram')
# plt.xlabel('# of Occurances')
# plt.show()
#
#
# true_word = basic_clean(''.join(str(true['text'].tolist())))
# #
# true_bigrams_series = (pd.Series(nltk.ngrams(true_word, 3)).value_counts())[:20]
# true_bigrams_series.sort_values().plot.barh(color='blue',  width=.9, figsize=(12, 8))
# plt.title('20 Most Frequently Occuring Trigrams')
# plt.ylabel('Trigram')
# plt.xlabel('# of Occurances')
# plt.show()
#
#
true_word = basic_clean(''.join(str(fake['text'].tolist())))

true_bigrams_series = (pd.Series(nltk.ngrams(true_word, 3)).value_counts())[:20]
true_bigrams_series.sort_values().plot.barh(color='blue', width=.9, figsize=(12, 8))
plt.title('20 Most Frequently Occuring Trigrams')
plt.ylabel('Trigram')
plt.xlabel('# of Occurances')
plt.show()
#
# show_wordcloud(train['text'])
